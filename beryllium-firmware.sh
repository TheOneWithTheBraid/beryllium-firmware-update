#!/usr/bin/env sh

BOARD="/proc/device-tree/compatible"
if [[ ! -e "$BOARD" ]] || [[ -z "$(grep "beryllium" $BOARD)" ]] ; then
	echo "This script will only run on xiaomi-beryllium, I'm very certain."
	exit 1
fi


# we will require root here

if [ "$(id -u)" != "0" ]; then
	echo "Are you root ?"
	exit 1
fi


export FILE="$1"
export SLOT="$2"

if [[ ! -e "$FILE" ]] ; then
	echo "Please provide a path to the vendor firmware zip file."
	exit 1
fi

if [[ ! "a" = "$SLOT" ]] && [[ ! "b" = "$SLOT" ]] && [[ ! "both" = "$SLOT" ]] ; then
	echo "The boot slot provided must be a, b or both."
	exit 1
fi


function flash_partition() {
	PARTITION="$1"
	BINARY="$2"

	OUTPUTS=""

	if [[ -e "/dev/disk/by-partlabel/${PARTITION}_a" ]] && [[ -e "/dev/disk/by-partlabel/${PARTITION}_b" ]] ; then
		if [[ "both" = "$SLOT" ]]; then
			OUTPUTS="/dev/disk/by-partlabel/${PARTITION}_a /dev/disk/by-partlabel/${PARTITION}_b"
		else
			OUTPUTS="/dev/disk/by-partlabel/${PARTITION}_${SLOT}"
		fi
	else
		OUTPUTS="/dev/disk/by-partlabel/${PARTITION}"
	fi

	for OUTPUT in $OUTPUTS ; do
		dd if="$BINARY" of="$OUTPUT" bs=4k &>/dev/null ; sync "$OUTPUT" 
	done
	
	echo "Done flashing parition ${PARTITION}."
}

export TMP="$(mktemp -d)"

unzip -qq "$FILE" "firmware-update/*" -d "$TMP"

export FIRMWARE="$TMP/firmware-update"

flash_partition abl "$FIRMWARE/abl.elf"
flash_partition aop "$FIRMWARE/aop.img"
flash_partition bluetooth "$FIRMWARE/bluetooth.img"
flash_partition cmnlib "$FIRMWARE/cmnlib.img"
flash_partition cmnlib64 "$FIRMWARE/cmnlib64.img"
flash_partition devcfg "$FIRMWARE/devcfg.img"
flash_partition dsp "$FIRMWARE/dsp.img"
flash_partition hyp "$FIRMWARE/hyp.img"
flash_partition keymaster "$FIRMWARE/keymaster.img"
flash_partition logo "$FIRMWARE/logo.img"
flash_partition modem "$FIRMWARE/modem.img"
flash_partition qupfw "$FIRMWARE/qupfw.img"
flash_partition storsec "$FIRMWARE/storsec.img"
flash_partition tz "$FIRMWARE/tz.img"
flash_partition xbl "$FIRMWARE/xbl.img"
flash_partition xbl_config "$FIRMWARE/xbl_config.img"

echo "All partitions flashed. You should really reboot now."

rm -rf "$TMP"

