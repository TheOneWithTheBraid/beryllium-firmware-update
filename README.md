# beryllium firmware update

Flash new vendor firmware onto the Xiaomi Pocophone F1 from your Linux mobile on !

No fastboot required ! Works 100 % from your running Linux system on.

## Find the latest firmware package

For beryllium, as well as many other Xiaomi devices, there is a collection of firmware downloads available at https://xiaomifirmwareupdater.com/firmware/beryllium/ . Please chose the latest version matching your modem region.

Alternative archive link for global modem (better use the latest one) : [GitHub archive link (do not use)](https://github.com/XiaomiFirmwareUpdaterReleases/firmware_xiaomi_beryllium/releases/download/stable-12.01.2021/fw_beryllium_miui_POCOF1Global_V12.0.3.0.QEJMIXM_cf3fccffce_10.0.zip)

## Execute

```shell
# flash to partition slot a
./beryllium-firmware.sh /path/to/vendor/firmware.zip a
# flash to partition slot b
./beryllium-firmware.sh /path/to/vendor/firmware.zip b
# flash to both slots
./beryllium-firmware.sh /path/to/vendor/firmware.zip both
```

## Technical background

Qualcomm SoCs (as much as many other embedded SoCs) contain single partition for all firmware related information : firmware itself, Device Tree Overlays, recovery, modem data and even key material.

Those are technically only labeled GPT partitions accessible by keyword known from fastboot flash commands.

This script uses these keywords to flash the vendor provided firmware onto the paritions, respectively partition a and b or both is requested.

## Adoption

The same script with likely identical or highly similar partitions might be used by many other devices too. Feel free to tinker but be careful. You are directly flashing onto your device's partition table !

## LICENSE

This software is issued under the terms and conditions of EUPL-1.2.

